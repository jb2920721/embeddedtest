#include <stdio.h>
#include <stdlib.h>
#include <pd9982_driver.h>

#include "include/display_handler.h"
#include "include/pd9982.h"
#include "include/buffer_handler.h"

int main(int argc, char *argv[])
{
    volatile struct pd9982_display *pd = 
        (volatile struct pd9982_display *)initModule("output.txt");

    struct textbuffer textbuffer;

    if (argc == 2) {
        if (read_text2display_file(&textbuffer, argv[1])) {
            printf("Failed to read data from file: %s\n", argv[1]);
            exit(1);
        }
    } else {
        printf("Please supply a single input-file in utf-8 format\n");
        printf("Continuing with example input\n");
        if (read_text2display_default(&textbuffer)) {
            printf("Failed to read dummy data\n");
            exit(1);
        }
    }

    if (pd == NULL) {
        printf("Failed to initilize module!\n");
        exit(1);
    }

    startModule();
    printf("Starting module\n");
    while (!(pd->pd_sts & START_BIT)) {
    }
    printf("Display started!\n");

    display_textbuffer(pd, &textbuffer);

    stopModule();
    free_buffer(&textbuffer);

    printf("Done!\n");
    return 0;
}
