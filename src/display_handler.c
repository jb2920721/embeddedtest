#include "include/display_handler.h"
#include "include/buffer_handler.h"
#include "include/pd9982.h"
#include <stdio.h>
#include "include/crc_calc.h"

/**
 * Checks if given value can be interpreted as a UTF-16 surrogate
 **/
static int is_surrogate(uint16_t value)
{
    return ((0xD800 <= value) && (value <= 0xDBFF));
}

/**
 * Busy waits for sts busy bit to be set to 1
 **/
static void wait_for_busy_on(volatile struct pd9982_display *pd)
{
    while(!(pd->pd_sts & STS_BSY_BIT)) {
    }
}

/**
 * Busy waits for sts busy bit to be set to 0
 **/
static void wait_for_busy_off(volatile struct pd9982_display *pd)
{
    while ((pd->pd_sts & STS_BSY_BIT)) {
    }
}

/**
 * Send reset command to the pd9982, then busy wait for it to be reset
 **/
static void handle_crc_err(volatile struct pd9982_display *pd)
{
    pd->pd_ctrl = RESET_CMD;

    printf("CRC error\n");

    while(pd->pd_sts & STS_CRC_ERR) {
    }

    wait_for_busy_off(pd);
}

/**
 * Try to sen num bytes to the pd9982, return the number of bytes actually transferred
 **/
static int send_bytes(volatile struct pd9982_display *pd, int num)
{
    uint8_t num_trans = 0;
    pd->crc = calc_crc_fast(&pd->txbuf[0], (num & CNT_MASK));
    pd->pd_ctrl = ((num & CNT_MASK) << CTRL_CNT_BIT);

    wait_for_busy_on(pd);
    pd->pd_ctrl = 0;

    wait_for_busy_off(pd);
    if (pd->pd_sts & STS_CRC_ERR) {
        handle_crc_err(pd);
        /* No bytes were transferred if CRC was wrong */
        return 0;
    }

    num_trans = (pd->pd_sts >> STS_CNT_BIT) & CNT_MASK;
    return num_trans;
}

/**
 * Start conversion of display buffer to UTF-8 and display the result
 * busy waits until it's done
 **/
static void flush_display(volatile struct pd9982_display *pd)
{
    wait_for_busy_off(pd);
    pd->pd_ctrl = SHIFT_CMD;
    wait_for_busy_on(pd); 
    pd->pd_ctrl = 0;
    wait_for_busy_off(pd);
}

int display_textbuffer(volatile struct pd9982_display *pd,
                       struct textbuffer *buffer)
{
    int bytes_to_send = 0, bytes_left = 0, bytes_sent = 0;
    int pdbuff_cnt = 0, left_in_pdbuff = 0;
    int should_flush = 0;

    buffer->num_sent = 0;
    while (buffer->num_sent != buffer->size) {

        bytes_left = buffer->size - buffer->num_sent;

        /* Make sure we are never trying to send more bytes than the tx buffer allows*/
        bytes_to_send = (bytes_left > TXBUF_SZ) ? TXBUF_SZ : bytes_left;

        /* Calculate how much there is left in the dispaly buffer to make sure we don't overflow */
        left_in_pdbuff = DISPLAY_BUFFER_SZ - (pdbuff_cnt+bytes_to_send);

        uint8_t last_sent_byte = pd->txbuf[bytes_sent-1];

        /* Can't trim when busy */
        /* Update tx buffer */
        wait_for_busy_off(pd);
        trim_buf(pd, buffer);

        if (left_in_pdbuff <= TXBUF_SZ) {
            uint8_t bytepair_bytes[2];
            int length_adjust = 0;

            /* This is the last itteration before we need to flush the */
            /* internal display buffer, and as the display will convert */
            /* whatever is in the buffer we need to make sure that a */
            /* valid bytepair is present, i.e. not end on a surrogate. */
            /* So check if we are sending a surrogate, in which case we */
            /* send all 4 bytes of it. Else just send 2 bytes */
            /* If the number of bytes sent are odd then we know the last */
            /* byte sent was paired with the first one in this batch. */
            /* So we need to rebuild it here before checking if that was a */
            /* surrogate. We also need to compensate for that byte in the */
            /* length of this batch */
            if ((buffer->num_sent % 2) != 0) {
                bytepair_bytes[0] = last_sent_byte;
                bytepair_bytes[1] = pd->txbuf[0];
                length_adjust = 1;
            } else {
                bytepair_bytes[0] = pd->txbuf[0];
                bytepair_bytes[1] = pd->txbuf[1];
                length_adjust = 0;
            }
            uint16_t *prev_bytepair = (uint16_t *)&bytepair_bytes[0];
            if (is_surrogate(*prev_bytepair)) {
                bytes_to_send = 4;
            } else {
                bytes_to_send = 2;
            }
            bytes_to_send -= length_adjust;

            should_flush = 1;
        }

        /* This loop is to make sure the number of bytes we want to send is actually sent */
        /* Because it is not guaranteed with CRC errors and the display beeing busy*/
        bytes_sent = 0;
        while ((bytes_sent != bytes_to_send)) {
            int diff = bytes_to_send - bytes_sent;

            /* Can't trim when busy */
            wait_for_busy_off(pd);
            trim_buf(pd, buffer);

            int n = send_bytes(pd, diff);
            bytes_sent += n;
            buffer->num_sent += n;
        }

        /* Update the counter so we can keep track of how full the display buffer is */
        pdbuff_cnt += bytes_sent;

        if ((pdbuff_cnt == DISPLAY_BUFFER_SZ) || should_flush) {
            flush_display(pd);
            pdbuff_cnt = 0;
            should_flush = 0;
        }
        printf("Sent %i/%i bytes\n", buffer->num_sent, buffer->size);
    }

    /* If there still is data that hasn't been flushed, flush it now */
    if (pdbuff_cnt) {
        flush_display(pd);
    }

    printf("Transferred %i bytes\n", buffer->num_sent);
    return 0;
}
