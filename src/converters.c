#include "include/converters.h"

#include <stdio.h>
#include <iconv.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>

/**
 * Prints the error we got from iconv
 **/
static void print_iconv_error(int error)
{
    switch (error) {
	    case EILSEQ:
	        printf ("Invalid multibyte sequence.\n");
	        break;
	    case EINVAL:
	        printf ("Incomplete multibyte sequence.\n");
	        break;
	    case E2BIG:
	        printf ("No more room.\n");
	        break;
	    default:
	        printf ("Error: %s.\n", strerror (errno));
	}
    
}

int utf8_to_utf16le(char *in_str, char *out_str, int len)
{
    char *inptr;
    char *outptr;
    iconv_t cd;
    size_t inleft;
    size_t outleft;
    int res;

    cd = iconv_open("UTF-16LE", "UTF-8");
    if (cd == (iconv_t)(-1)) {
        printf("Failed to start converter!\n");
        return 1;
    }

    inleft = len;

    /* Compensate for the fact that UTF8 and UTF16 are different sizes */
    outleft = len*2;
    inptr = (char *)in_str;
    outptr = (char*)out_str;

    res = iconv(cd, &inptr, &inleft, &outptr, &outleft);
    if (res == -1) {
        printf("Error in converting characters\n");
        print_iconv_error(errno);
        return -1;
    }
    iconv_close(cd);
    return len*2 - outleft;
}

int utf16le_to_utf8(char *in_str, char *out_str, int len)
{
    char *inptr;
    char *outptr;
    iconv_t cd;
    size_t inleft;
    size_t outleft;
    int res;

    cd = iconv_open("UTF-16LE", "UTF-8");
    if (cd == (iconv_t)(-1)) {
        printf("Failed to start converter!\n");
        return 1;
    }

    inleft = len;

    /* Compensate for the fact that UTF16 and UTF8 are different sizes */
    outleft = len*4;
    inptr = (char *)in_str;
    outptr = (char*)out_str;

    res = iconv(cd, &inptr, &inleft, &outptr, &outleft);
    if (res == -1) {
        printf("Error in converting characters\n");
        print_iconv_error(errno);
        return -1;
    }
    iconv_close(cd);
    return len*4 - outleft;
}
