#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "include/buffer_handler.h"
#include "include/converters.h"

int free_buffer(struct textbuffer *buffer){
    if (buffer->text2display){
        free(buffer->text2display);
        buffer->text2display = NULL;
    }
    return 0;
}

/**
 * Get the size of file
 **/
static int get_file_size(FILE *fp){
    int prev=ftell(fp);
    fseek(fp, 0L, SEEK_END);
    int sz=ftell(fp);
    fseek(fp,prev,SEEK_SET);
    return sz;
}

int read_text2display_default(struct textbuffer *buffer)
{
    char *str = "🌂🌞🌂🦞🌂💺🌂🌞🌂🦞🌂💺🌂";
    buffer->text2display = malloc(strlen(str));
    int sz = utf8_to_utf16le(str, buffer->text2display, strlen(str));
    
    if (sz == -1)
        return 1;

    buffer->size = sz;
    return 0;
}

int read_text2display_file(struct textbuffer *buffer, char *name)
{
    char *content;
    int filesz = 0;
    FILE *fptr;
    fptr = fopen(name, "r");
    filesz = get_file_size(fptr);
    content = malloc(filesz + 1);
    buffer->text2display = malloc(filesz * 2);
    fgets(content, filesz + 1, fptr);
    fclose(fptr);

    int sz = utf8_to_utf16le(content, buffer->text2display, filesz);
    free(content);
    if (sz == -1)
        return 1;

    buffer->size = sz;
    return 0;
}

void trim_buf(volatile struct pd9982_display *pd,
              struct textbuffer *buffer)
{
    int i = 0;
    int dif = buffer->size - buffer->num_sent;
    if (dif > TXBUF_SZ)
        dif = TXBUF_SZ;

    for(i=0;i<=dif;i++) {
        pd->txbuf[i] = buffer->text2display[buffer->num_sent + i];
    }
}
