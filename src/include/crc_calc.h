#ifndef _CRC_CALC
#define _CRC_CALC

#include <stdint.h>

/** @brief Calculates CRC
 *
 *  @param buf pointer to data
 *  @param len length of the data
 *  @return Calculated CRC value
 */
uint8_t calc_crc(uint8_t *buf, uint8_t len);

/** @brief Calculates CRC
 *
 *  Calculates CRC using CRC table
 *
 *  @param buf pointer to data
 *  @param len length of the data
 *  @return Calculated CRC value
 */
uint8_t calc_crc_fast(volatile uint8_t *buf, uint8_t len);

#endif /* _CRC_CALC */
