
#ifndef _DISPLAY_HANDLER_H
#define _DISPLAY_HANDLER_H

#include "pd9982.h"
#include "buffer_handler.h"

/** @brief Prints textbuffer to display
 *
 *  Takes textbuffer data and, splits it into smaller chunks
 *  and transfers it to the display
 *
 *  @param pd Pointer to pd9982 display
 *  @param buffer Pointer to textbuffer
 *  @return 0 on success
 */
int display_textbuffer(volatile struct pd9982_display *pd,
                       struct textbuffer *buffer);

#endif /* _DISPLAY_HANDLER_H */
