#ifndef  _CONVERTERS_H
#define  _CONVERTERS_H

/** @brief Converts text to utf-8
 *
 *
 *  @param in_str String to be converted
 *  @param out_str Pointer to where to store the result
 *  @param len Length of the string
 *  @return lenght of the converted string on succes, else -1
 */
int utf16le_to_utf8(char *in_str, char *out_str, int len);

/** @brief Converts text to utf-8
 *
 *
 *  @param in_str String to be converted
 *  @param out_str Pointer to where to store the result
 *  @param len Length of the string
 *  @return lenght of the converted string on succes, else -1
 */
int utf8_to_utf16le(char *in_str, char *out_str, int len);

#endif /* _CONVERTERS_H */
