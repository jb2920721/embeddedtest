#ifndef _BUFFER_HANDLER_H
#define _BUFFER_HANDLER_H

#include "pd9982.h"

struct textbuffer {
    char *text2display; /* Text to display on the display */
    int size; /* Size of the text in bytes */
    int num_sent; /* Number of bytes already transferred */
};

/** @brief Trims pd buffer
 *
 *  Updates pd9982 tx buffer with the next data to display
 *  from the textbuffer
 *
 *  @param pd Pointer to pd9982 display
 *  @param buffer Pointer to textbuffer
 */
void trim_buf(volatile struct pd9982_display *pd,
              struct textbuffer *buffer);

/** @brief Free textbuffer data
 *
 *  @param buffer Pointer to textbuffer
 *  @return always 0
 */
int free_buffer(struct textbuffer *buffer);

/** @brief Read textbuffer data from file
 *
 *  Reads data from a file, converts it to UTF-16LE and
 *  updates the textbuffer struct with data and lendth
 *
 *  @param buffer Pointer to textbuffer
 *  @param name Filename to read
 *  @return 0 on success else 1
 */
int read_text2display_file(struct textbuffer *buffer, char *name);

/** @brief Read dummy data to textbuffer
 *
 *  Reads dummy data, converts it to UTF-16LE and
 *  updates the textbuffer struct with data and lendth
 *
 *  @param buffer Pointer to textbuffer
 *  @param name Filename to read
 *  @return 0 on success else 1
 */
int read_text2display_default(struct textbuffer *buffer);

#endif /* _BUFFER_HANDLER_H */
