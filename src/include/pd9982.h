#ifndef _PD9982_H
#define _PD9982_H

#include <stdint.h>

#define TXBUF_SZ 7

struct pd9982_display {
    uint8_t pd_ctrl;
    uint8_t pd_sts;
    uint8_t txbuf[TXBUF_SZ];
    uint8_t crc;
};

#define START_BIT        (1U << 2)
#define SHIFT_CMD         0xF1U
#define RESET_CMD        (1U << 7)
#define STS_CRC_ERR      (1U << 7)
#define STS_CNT_BIT       4U
#define STS_BSY_BIT       1U
#define CTRL_CNT_BIT      1U

#define CNT_MASK          0x7U
#define DISPLAY_BUFFER_SZ 64U

#endif /* _PD9982_H */
