# Embedded test
This is my submission for the Embedded code test.
It assumes you are running linux on a little endian system, sorry SPARC users...

# To build
    > make
This will build an application called "app"

# To run
    > ./app [filename]
If a file is supplied it is assumed it is UTF-8 formated text, only 1 file
can be supplied. If no file is specified it will run on a default testpattern.
The output will be generated in a file called "output.txt"

# To test
    > make runtests
This will run the application on all files in the "test" folder ending in
.txt. It will then compare the generated "output.txt" SHA256 sum with the
SHA256 of the input file. It can be a bit messy so I made the script
"test_filter.sh" that will filter the output for more user friendly experience.

    > make runtests | ./test_filter.sh

It takes around 2min on my machine.


# "Future" improvments
This solution makes use of the entire 7 bit tx buffer on the PD9982 display
except for the last itteration of the when the intenal buffer start to get full.
Then it will only send 1 character (max 4 bytes if it is a surrogate or max 2
bytes if it's not) this can be optimised to include another character in some
cases.
