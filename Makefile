CC=gcc
CFLAGS=
INCLUDE=./lib/include
LIBS=./lib/libpd9982-linux.a

APP_SOURCES=./src/*.c

BINARIES=app

TEST_INPUTS=./test/*.txt

app: $(APP_SOURCES)
	$(CC) $(CFLAGS) -I$(INCLUDE) $^ $(LIBS) -o $@

.PHONY: runtests
runtests: app
	for t in $(TEST_INPUTS); \
		do \
		./app $$t; \
		echo "RESULT: $$(echo "$$(sha256sum output.txt | gawk '{ print $$1 }') $$t" | sha256sum --check)"; \
		done

.PHONY: clean
clean:
	rm $(BINARIES) output.txt
