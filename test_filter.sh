#!/bin/bash

NORMAL="\033[0m"
GREEN="\033[0;32m"
RED="\033[0;31m"
BOLD="\033[1m"

grep --line-buffered "^RESULT: " | \
    gawk -v r=$RED -v g=$GREEN -v n=$NORMAL -v b=$BOLD \
    '{if ($3 == "OK") { print $2 " " g b $3 n } \
     else {print $2 " " r b $3 n }}' \
